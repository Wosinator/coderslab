package a_Zadania.b_Dzien_2.a_Napisy;

public class Main4 {

    public static void main(String[] args) {
        String test = "TaK";
        System.out.println(toggleChar(test));
    }

    static String toggleChar(String str) {
        char[] tab = str.toCharArray();
        char[] toggled = new char[tab.length];
        for (int i = 0; i < tab.length ; i++) {
            char currentChar = tab[i];
            if(Character.isLowerCase(currentChar)){
                 currentChar = Character.toUpperCase(currentChar);
            } else {
                currentChar = Character.toLowerCase(currentChar);
            }
            toggled[i] = currentChar;
        }
        return String.valueOf(toggled);
    }
}
