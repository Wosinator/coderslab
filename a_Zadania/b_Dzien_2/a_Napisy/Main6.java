package a_Zadania.b_Dzien_2.a_Napisy;

public class Main6 {

    public static void main(String[] args) {

        String test = "CodersLab";
        System.out.println(firstHalf(test));

    }

    static String firstHalf(String str){

        return str.substring(0, str.length()/2);

    }
}
