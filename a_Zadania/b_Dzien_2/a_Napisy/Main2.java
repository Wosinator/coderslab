package a_Zadania.b_Dzien_2.a_Napisy;

public class Main2 {

    public static void main(String[] args) {

        String str = "codersLabxyz";
        String pallindrome = "kaak";
        System.out.println(isPallindrome(str));
        System.out.println(isPallindrome(pallindrome));

    }

    static boolean isPallindrome(String test) {
        String whiteSpaces = test.replaceAll(" ","");
        for (int i = 0; i < whiteSpaces.length()/2 ; i++) {
            if(whiteSpaces.charAt(i) != whiteSpaces.charAt((whiteSpaces.length()-1)-i)) {
                return false;
            }
        }
        return true;
    }

}
