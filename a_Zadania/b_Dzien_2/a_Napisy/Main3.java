package a_Zadania.b_Dzien_2.a_Napisy;

public class Main3 {

    public static void main(String[] args) {

    String str = "codersLabxyz";
        System.out.println(reverse(str));
    }

    static String reverse(String a) {
        char[] tablica = a.toCharArray();
        char[] reversed = new char[tablica.length];
        int j =0;
        for (int i = tablica.length-1; i >= 0 ; i--) {
            reversed[j] = tablica[i];
            j++;
        }
        return String.valueOf(reversed);
    }
}
