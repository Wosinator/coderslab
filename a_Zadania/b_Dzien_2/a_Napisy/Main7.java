package a_Zadania.b_Dzien_2.a_Napisy;

public class Main7 {

    public static void main(String[] args) {

        String codersLab = "CodersLab";
        System.out.println(containsStr(codersLab, "Code"));

    }

    static boolean containsStr(String str, String search){
       return str.contains(search);
    }
}
