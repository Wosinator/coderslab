package a_Zadania.b_Dzien_2.b_Podzial_Napisow;


import java.util.StringTokenizer;

public class Main4 {

    public static void main(String[] args) {
        String test = "Pies-Kot-Swinka-Delfin";
        String[] first = onlyTwoElementsWithSplit(test, '-');
        String[] second = getWithSplit(test, '-');
        for (String element: first) {
            System.out.println(element);
        }
        for (String element: second) {
            System.out.println(element);
        }
    }

    static String[] onlyTwoElementsWithSplit(String str, char separaor) {
        int finalIndex = -1;
        for (int i = 0; i <str.length() ; i++) {
            if(str.charAt(i) == separaor){
                finalIndex = i;
                break;
            }
        }
        String[] strings = new String[2];
        strings[0] = str.substring(0, finalIndex);
        strings[1] = str.substring(finalIndex+1);
        return strings;
   }
   static String[] getWithSplit(String str, char separator){
    return str.split(String.valueOf(separator), 1);
    }

}