package a_Zadania.b_Dzien_2.b_Podzial_Napisow;


import java.util.StringTokenizer;

public class Main3 {

    public static void main(String[] args) {
        String test = " Ala  ma kota a kot ma ale w tym zdaniu nie ma przecinków.";
        System.out.println(countWithSplit(test));
        System.out.println(countWithTokenizer(test));
    }


    static int countWithSplit(String str) {
        String[] strings = str.trim().split(" +");
        return strings.length;
    }

    static int countWithTokenizer(String str) {
        StringTokenizer tokenizer = new StringTokenizer(str);
        return tokenizer.countTokens();
    }

}