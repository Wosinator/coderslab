package a_Zadania.b_Dzien_2.b_Podzial_Napisow;


import java.util.StringTokenizer;

public class Main2 {

    public static void main(String[] args) {

    String str = "Z Coders Lab niezależnie od wykształcenia możesz zmienić swoją karierę." +
            " Jesteśmy szkołą oferującą bardzo intensywne kursy programowania." +
            " Przygotujemy Cię do pracy na stanowisku junior web developera i pomożemy znaleźć zatrudnienie po kursie.";
        splitWithSplit(str);
        splitWithTokenizer(str);
    }

    static void splitWithSplit(String str) {
        String[] strings = str.split("[.]");
        for(String line : strings) {
            System.out.println(line);
        }
    }

    static void splitWithTokenizer(String str) {
        StringTokenizer tokenizer = new StringTokenizer(str,".",true);
        while (tokenizer.hasMoreElements()) {
            System.out.println(tokenizer.nextToken() + tokenizer.nextToken());

        }
    }
}
