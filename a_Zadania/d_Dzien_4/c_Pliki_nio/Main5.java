package a_Zadania.d_Dzien_4.c_Pliki_nio;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main5 {

    public static void main(String[] args) {
        try {
            readFromFile("text1.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void readFromFile(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        List<String> lista = new ArrayList<>();
        lista.add("<html>");
        lista.add("<body>");
        for(String line : Files.readAllLines(path)) {
            lista.add("<p>" + line + "</p>");
        }
        String noExtension = fileName.split("[.]")[0];
        Path pathHtml = Paths.get(noExtension +".html");
        lista.add("</body>");
        lista.add("</html>");
        Files.write(pathHtml, lista);
    }

}
