package a_Zadania.d_Dzien_4.c_Pliki_nio;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main3 {

    public static void main(String[] args) {

        copyFile("test", "pieski", "pieski2");
    }

    static void copyFile(String directory, String fileName, String fileName2) {
        Path path1 = Paths.get(directory, fileName);
        Path path2 = Paths.get(directory, fileName2);

        if(Files.exists(path1)){
            try {
                Files.copy(path1, path2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Plik nie istnieje!");
        }
    }


}
