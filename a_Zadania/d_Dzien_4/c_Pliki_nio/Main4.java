package a_Zadania.d_Dzien_4.c_Pliki_nio;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main4 {

    public static void main(String[] args) {
        writeToFile("piesulki.txt");
    }

    static void writeToFile(String fileName) {
        Path path = Paths.get(fileName);

        List<String> lista = new ArrayList<>();
        try(Scanner scanner = new Scanner(System.in)) {

            if (!Files.exists(path)) {
                Files.createFile(path);
            }
            boolean keepWorking = true;
            while (keepWorking) {
                String current = scanner.nextLine();
                if (current.toLowerCase().equals("quit")) {
                    keepWorking = false;
                } else {
                    lista.add(current);
                }
            }
            Files.write(path, lista);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
