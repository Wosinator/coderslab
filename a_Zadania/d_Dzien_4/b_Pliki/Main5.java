package a_Zadania.d_Dzien_4.b_Pliki;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main5 {

    public static void main(String[] args) throws IOException {
        File file = new File("./a_Zadania/d_Dzien_4/b_Pliki/text2.txt");
        StringBuilder builder = new StringBuilder();
        Scanner scanner = new Scanner(file);

        while(scanner.hasNextLine()) {
            builder.append(scanner.nextLine()).append(",");
        }

        String[] toSort = builder.toString().split(",");
        Arrays.sort(toSort);
        System.out.println(Arrays.toString(toSort));
    }
}
