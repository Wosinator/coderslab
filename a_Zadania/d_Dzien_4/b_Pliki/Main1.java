package a_Zadania.d_Dzien_4.b_Pliki;


import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Main1 {

    public static void main(String[] args) {
        fileWrite();
        File file = new File("C:/");
        System.out.println(Arrays.toString(file.listFiles((a,b) -> b.endsWith("jpg")))) ;
    }

    static void fileWrite() {
        try (PrintWriter writer = new PrintWriter("Main1.txt");Scanner scanner = new Scanner(System.in)) {
            boolean keepWorking = true;
            while (keepWorking) {
                String current = scanner.nextLine();
                if (current.toLowerCase().equals("quit")) {
                    keepWorking = false;
                } else {
                    writer.println(current);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Problem z plikiem");
        }
    }
}
