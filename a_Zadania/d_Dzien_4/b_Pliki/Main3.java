package a_Zadania.d_Dzien_4.b_Pliki;


import sun.util.locale.LocaleUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Main3 {

    public static void main(String[] args) throws FileNotFoundException {
        double sum = 0.0;
        File file = new File("Main1.txt");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] splitLine = line.split(",");
            for(String element: splitLine){
                double numberParsed = safeStringToDouble(element);
                sum += numberParsed;
            }
        }
        System.out.println(sum);
        }

        public static double safeStringToDouble(String string) {
            try{
                return Double.valueOf(string);
            } catch (NumberFormatException ex) {
                System.out.println("Not a number");
                return 0;
            }
        }
    }

