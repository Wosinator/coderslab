package a_Zadania.d_Dzien_4.a_Wprowadzanie_danych;


import java.util.Scanner;

public class Main7 {

    public static void main(String[] args) {

        nettoBrutto();
    }

    static void nettoBrutto() {
        Scanner scanner = new Scanner(System.in);
        String operationType = "";
        while(!operationType.equals("bn") && !operationType.equals("nb")) {
            System.out.println("Podaj typ operacji! :");
            operationType = scanner.nextLine();
        }
        System.out.println("Podaj liczbę do zamiany: ");
        while(!scanner.hasNextDouble()) {
            scanner.next();
            System.out.println("To nie liczba :<");
        }
        double toChange = scanner.nextDouble();
        if(operationType.equals("bn")){
            System.out.println("Value net: " + toChange/1.23);
        } else {
            System.out.println("Value gross:" + toChange*1.23);
        }
    }
}
