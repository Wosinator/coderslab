package a_Zadania.d_Dzien_4.a_Wprowadzanie_danych;


import java.util.Scanner;

public class Main5 {

    public static void main(String[] args) {
        textLines();

    }

    static void textLines() {
        Scanner scanner = new Scanner(System.in);
        boolean keepWorking = true;
        while (keepWorking) {
            String current = scanner.nextLine();
            if(current.equals("quit")){
                keepWorking = false;
            } else {
                System.out.println(current);
            }
        }
    }
}
