package a_Zadania.d_Dzien_4.a_Wprowadzanie_danych;


import java.util.Scanner;

public class Main6 {

    public static void main(String[] args) {
        equation();

    }

    static void equation() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj a: ");
        while(!scanner.hasNextInt()) {
            scanner.next();
            System.out.println("To nie liczba :<");
        }
        int a = scanner.nextInt();
        System.out.println("Podaj b: ");

        while(!scanner.hasNextInt()) {
            scanner.next();
            System.out.println("To nie liczba :<");
        }
        int b = scanner.nextInt();
        System.out.println("Podaj c: ");

        while(!scanner.hasNextInt()) {
            scanner.next();
            System.out.println("To nie liczba :<");
        }
        int c = scanner.nextInt();
        double delta = (b*b) - (4 * a * c);
        if( delta > 0){
            System.out.println("Delta dodatnia!");
            double root1 = (-b - Math.sqrt(delta))/ (2*a);
            double root2 = (-b + Math.sqrt(delta))/ (2*a);
            System.out.println("Root 1: " + root1);
            System.out.println("Root 2: " + root2);
        } else if (delta == 0) {
            double root = -b/(2*a);
            System.out.println("Root: " + root );
        } else {
            System.out.println("Delta Ujemna :<");
        }
    }
}
