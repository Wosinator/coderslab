package a_Zadania.d_Dzien_4.d_Eclipse_jar;


import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public class Main3 {

    public static void main(String[] args) {
        String[] array = {"Pieski", "Kotki", "Swinki"};
        String joined= stringFromArray(array);
        System.out.println(joined);
    }

    static String stringFromArray(String[] array) {
        return StringUtils.join(array);
    }

}
