package a_Zadania.a_Dzien_1.b_Metody;


public class Main9 {

    public static void main(String[] args) {
        System.out.println(factorial(5));

    }

    static int factorial(int a) {
        int result = 1;
        for (int i = a ; i > 0 ; i--) {
            result *= i;
        }
        return result;
    }
}
