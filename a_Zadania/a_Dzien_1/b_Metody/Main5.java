package a_Zadania.a_Dzien_1.b_Metody;


public class Main5 {

    public static void main(String[] args) {

        System.out.println(getNetto(100, 0.23));
    }

    static double getNetto(double price, double tax) {
        double netPercent = 1.0 - tax;
        return price * netPercent;
    }
}
