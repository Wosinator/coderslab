package a_Zadania.a_Dzien_1.b_Metody;


public class Main10 {

    public static void main(String[] args) {
        System.out.println(footballWin(0,2,3,1));

    }

    static String footballWin(int teamAHome, int teamBAway, int teamAAway, int teamBHome) {
        int teamAScore = teamAAway + teamAHome;
        int teamBScore = teamBAway + teamBHome;
        if(teamAScore > teamBScore) {
            return "1";
        } else if (teamBScore > teamAScore) {
            return "2";
        } else {
            if(teamAAway > teamBAway) {
                return "1";
            } else if (teamBAway > teamAAway) {
                return "2";
            } else {
                return "x";
            }
        }
    }
}
