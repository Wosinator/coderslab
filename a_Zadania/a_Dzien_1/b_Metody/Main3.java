package a_Zadania.a_Dzien_1.b_Metody;


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Main3 {

    public static void main(String[] args) {
        System.out.println(convertToUsd(BigDecimal.valueOf(10)).floatValue());

    }

    static BigDecimal convertToUsd(BigDecimal number) {
       return number.divide(BigDecimal.valueOf(4.04), MathContext.DECIMAL128);
    }
}
