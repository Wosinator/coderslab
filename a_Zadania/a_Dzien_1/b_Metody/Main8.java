package a_Zadania.a_Dzien_1.b_Metody;


public class Main8 {

    public static void main(String[] args) {

        System.out.println(getMaxOfThree(1, 2 ,3));
    }

    static int getMaxOfThree(int a, int b, int c) {
        if(a > b) {
            if (a > c) {
                return a;
            } else {
                return c;
            }
        } else {
            if (b > c) {
                return b;
            } else {
                return c;
            }
        }
    }
}
