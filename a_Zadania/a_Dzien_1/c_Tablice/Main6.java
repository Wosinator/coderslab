package a_Zadania.a_Dzien_1.c_Tablice;


import java.util.Arrays;

public class Main6 {

    public static void main(String[] args) {

        int[] numbers = {10,9,8,7,6,5,4,3,2,1};
        int[] secondNumners = {20,30,40,50,60,70,80,90,100, 110};
        int[] sum = new int[10];
        for (int i = 0; i < numbers.length; i++) {
            sum[i] = numbers[i] + secondNumners[i];
        }
        System.out.println(Arrays.toString(sum));
    }
}
