package a_Zadania.a_Dzien_1.c_Tablice;


import java.util.Arrays;
import java.util.Random;

public class Main4 {

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8,9,10};

        int[] reversed = new int[10];
        int j =0;
        for (int i = arr.length-1; i >= 0 ; i--) {
            reversed[j] = arr[i];
            j++;
        }
        System.out.println(Arrays.toString(reversed));
    }
}
