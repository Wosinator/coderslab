package a_Zadania.a_Dzien_1.c_Tablice;


import java.util.Random;

public class Main2 {

    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[20];
        for (int i = 0; i < 20 ; i++) {
            arr[i] = random.nextInt(101);
        }
        int min = 101;
        for (int number: arr) {
            if(number < min) {
                min = number;
            }
        }
        System.out.println(min);

    }
}
