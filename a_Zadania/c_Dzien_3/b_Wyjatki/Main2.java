package a_Zadania.c_Dzien_3.b_Wyjatki;

public class Main2 {

    public static void main(String[] args) {
        int a = divide(Integer.MAX_VALUE,1);
        System.out.println(a);
    }

    static int divide (int a, int b) throws ArithmeticException{
        int result = Integer.MIN_VALUE;
        try {
            result = a/b;
        } catch (ArithmeticException exception) {
            System.out.println("Nie dziel przez 0:<");
        }
        return result;
    }
}
